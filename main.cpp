#include <windows.h>

template<typename T> void write( size_t address, T value ) {
	DWORD  oldVP;
	size_t len = sizeof( T );

	VirtualProtect( (void *)address, len, PAGE_EXECUTE_READWRITE, &oldVP );
	*(T *)address = value;
	VirtualProtect( (void *)address, len, oldVP, nullptr );
}

BOOL APIENTRY DllMain( HMODULE, DWORD dwReasonForCall, LPVOID ) {
	static auto samp_dll = (DWORD)GetModuleHandleA( "samp" );
	static auto isR1	 = *(unsigned char *)( samp_dll + 0x129 ) == 0xF4;

	if ( dwReasonForCall == DLL_PROCESS_ATTACH ) {
		write( samp_dll + ( isR1 ? 0x5F06C : 0x6240C ), '\xEB' );
	} else if ( dwReasonForCall == DLL_PROCESS_DETACH ) {
		write( samp_dll + ( isR1 ? 0x5F06C : 0x6240C ), '\x74' );
	}

	return TRUE;
}
